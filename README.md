# swabfinder

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

Swab Finder adalah sebuah aplikasi web yang dapat melayani pengguna untuk menemukan institusi kesehatan terdekat yang menyediakan
layanan pemeriksaan PCR dan jalur menuju institusi kesehatan tersebut. Swab Finder diharapkan mampu mempermudah masyarakat untuk
mendapatkan informasi mengenai hal tersebut karena pemeriksaan PCR untuk COVID-19 tentunya sangat penting dalam usaha mengurangi
penyebaran virus COVID-19 di Indonesia.

## Kelompok Siap Cerdas

- Abdurrahman Luqmanul Hakim - 1806141113
- Piawai Said Umbara - 1806235933
- Raul Arrafi Delfarra - 1806205142
- Willy Sandi Harsono - 1806205786

## Web App

https://swab-finder.herokuapp.com/

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/laymonage/django-template-heroku/-/commits/master
