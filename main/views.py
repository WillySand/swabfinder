from django.shortcuts import render
from .models import Measurement
from .forms import MeasurementModelForm
from geopy.geocoders import Nominatim
from geopy.distance import geodesic
from .utils import get_geo, get_center_coordinates, get_zoom
import folium

def home(request):
	# initial value
	location = None
	distance = None

	form = MeasurementModelForm(request.POST or None)
	geolocator = Nominatim(user_agent='main')

	ip = '72.14.207.99'
	country, city, lat, lon = get_geo(ip)
	destination = geolocator.geocode(city)

	# destination coordinates
	d_lat = lat
	d_lon = lon
	pointB = (d_lat, d_lon)

	# initial folium map
	m = folium.Map(width=700, height=700, location=get_center_coordinates(d_lat, d_lon), zoom_start=3)

	#location marker
	folium.Marker([d_lat, d_lon], tooltip='click here for more', popup=city['city'],
					icon=folium.Icon(color='purple')).add_to(m)

	if form.is_valid():
		instance = form.save(commit=False)
		location_ = form.cleaned_data.get('location')
		location = geolocator.geocode(location_)

		# location coordinates
		l_lat = location.latitude
		l_lon = location.longitude
		pointA = (l_lat, l_lon)

		distance = round(geodesic(pointA, pointB).km, 2)

		# folium map modification
		m = folium.Map(width=700, height=500, location=get_center_coordinates(l_lat, l_lon, d_lat, d_lon), zoom_start=get_zoom(distance))

		# destination marker
		folium.Marker([d_lat, d_lon], tooltip='click here for more', popup=city['city'],
					icon=folium.Icon(color='purple')).add_to(m)

		# location marker
		folium.Marker([l_lat, l_lon], tooltip='click here for more', popup=location,
					icon=folium.Icon(color='red', icon='cloud')).add_to(m)

		# draw the line
		line = folium.PolyLine(locations=[pointA, pointB], weight=7, color='blue')
		m.add_child(line)

		instance.location = location
		instance.distance = distance
		instance.save()

	m = m._repr_html_()

	context = {
		'location': location,
		'distance': distance,
		'form': form,
		'map': m,
	}

	return render(request, 'main/home.html', context)
