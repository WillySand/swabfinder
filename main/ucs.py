from queue import PriorityQueue
import random
class Graph:
    class Node:
        
        def __init__(self, name):
            self.name = name
            self.edges = []
            
        def __lt__(self,other):
            return self.name < other.name
        
        def __le__(self,other):
            return self.name <= other.name

        def __str__(self):
            return self.name
        
        def __repr__(self):
            return self.name.upper()
        
        def addEdge(self, edge):
            self.edges.append(edge)

        def getName(self):
            return self.name

        def getEdges(self):
            return self.edges
        
        def getNeighbors(self):
            lst = []
            for edge in self.edges:
                pair = edge.getPair(self.name)
                lst.append([pair,edge.getCost(),pair.getName()])
            return lst

        
    class Edge:
        
        def __init__(self, node1, node2, cost):
            self.node1 = node1
            self.node2 = node2
            self.cost = cost

        def getPair(self,nodeName):
            if(self.node1.getName() == nodeName):
                return self.node2
            return self.node1

        def getCost(self):
            return self.cost


    def __init__(self):
        self.nodes = {}
    
    def addNode(self, name):
        if name not in self.nodes:
            self.nodes[name] = self.Node(name)

    def addDoubleEdge(self, nodeName1, nodeName2, cost):

        self.addNode(nodeName1)
        self.addNode(nodeName2)
        node1 = self.nodes[nodeName1]
        node2 = self.nodes[nodeName2]
        edge = self.Edge(node1, node2, cost)

        node1.addEdge(edge)
        node2.addEdge(edge)
        
    def addEdge(self, nodeName1, nodeName2, cost):

        self.addNode(nodeName1)
        self.addNode(nodeName2)
        node1 = self.nodes[nodeName1]
        node2 = self.nodes[nodeName2]
        edge = self.Edge(node1, node2, cost)
        node1.addEdge(edge)

    def pathFind(self, originName, destNames):
        
        queue=PriorityQueue()
        queue.put((0,self.nodes[originName],originName+""))
        visits = {}#Node:[costSum,path]
        
        while not queue.empty():
            current=queue.get()#(cost,node,path)
            
            if (current[1].getName() in destNames):
                return [current[0],current[2]]#cost and path

            if current[1] in visits :
                if current[0] < visits[current[1]][0]:    
                    visits[current[1]] = [current[0],current[2]]
                    
                else:
                    continue
            else:
                visits[current[1]]=[current[0],current[2]]
                
            print(current)#search process
            lst = current[1].getNeighbors()
            
            for i in lst: #i=[Node,int cost, str nodeName]
                try:
                    if visits[i[0]][0] > current[0]+i[1]:#stored cost > cost going in?
                        queue.put((i[1] + current[0], i[0], "{} {}".format(current[2],i[2])))
                        
                except:
                    queue.put((i[1] + current[0], i[0], "{} {}".format(current[2],i[2])))


"""
example:
x=Graph()
x.addEdge('a','b',2)
x.addEdge('a','c',3)
print("x.pathFind('a',['c'])")
print(x.pathFind('a',['c']))
x.addEdge('b','c',0)
x.addEdge('a','c',7)
x.addEdge('a','b',5)
print("x.pathFind('a',['c'])")
print(x.pathFind('a',['c']))
print("x.pathFind('b',['c'])")
print(x.pathFind('b',['c']))

x=Graph()
x.addEdge('a','b',3)
x.addEdge('a','d',2)
x.addEdge('a','i',4)
x.addEdge('h','b',4)
x.addEdge('h','c',2)
x.addEdge('d','c',6)
x.addEdge('f','c',1)
x.addEdge('f','g',8)
x.addEdge('d','e',1)
x.addEdge('i','e',8)
print(x.pathFind('a',['f','g']))

"""
