from django.db import models

class Measurement(models.Model):
	location = models.CharField(max_length=200)
	destination = models.CharField(max_length=200)
	distance = models.DecimalField(max_digits=10, decimal_places=2)
	path = models.CharField(max_length=200)

	def __str__(self):
		return f"The nearest from {self.location} is {self.destination} with distance: {self.distance} km and path: {self.path}"
