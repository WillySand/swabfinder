from queue import PriorityQueue
class Graph:
    class Node:
        
        def __init__(self, name):
            self.name = name
            self.edges = []
            
        def __lt__(self,other):
            return self.name < other.name
        
        def __le__(self,other):
            return self.name <= other.name

        def __str__(self):
            return self.name
        
        def __repr__(self):
            return self.name.upper()
        
        def addEdge(self, edge):
            self.edges.append(edge)

        def getName(self):
            return self.name

        def getEdges(self):
            return self.edges
        
        def getNeighbors(self):
            lst = []
            for edge in self.edges:
                pair = edge.getPair(self.name)
                lst.append([pair,edge.getCost(),pair.getName()])
            return lst

        
    class Edge:
        
        def __init__(self, node1, node2, cost):
            self.node1 = node1
            self.node2 = node2
            self.cost = cost

        def getPair(self,nodeName):
            if(self.node1.getName() == nodeName):
                return self.node2
            return self.node1

        def getCost(self):
            return self.cost

    def __init__(self):
        self.nodes = {}
        self.labs = {}
        
    def addLab(self, location, name):
        self.labs[location] = name

    def getLabLocations(self):
        return list(self.labs.keys())

    def getLabName(self, name):
        try:
            return self.labs[name]
        except:
            return "No Lab"
        
    def addNode(self, name):
        if name not in self.nodes:
            self.nodes[name] = self.Node(name)

    def addDoubleEdge(self, nodeName1, nodeName2, cost):

        self.addNode(nodeName1)
        self.addNode(nodeName2)
        node1 = self.nodes[nodeName1]
        node2 = self.nodes[nodeName2]
        edge = self.Edge(node1, node2, cost)

        node1.addEdge(edge)
        node2.addEdge(edge)
        
    def addEdge(self, nodeName1, nodeName2, cost):

        self.addNode(nodeName1)
        self.addNode(nodeName2)
        node1 = self.nodes[nodeName1]
        node2 = self.nodes[nodeName2]
        edge = self.Edge(node1, node2, cost)
        node1.addEdge(edge)

    def pathFind(self, originName, destNames):
        
        queue=PriorityQueue()
        try:
            queue.put((0,self.nodes[originName],originName+""))    
        except:
            print(originName +" tidak ditemukan")
            return [0.0,""]
        visits = {}#Node:[costSum,path]
        
        while not queue.empty():
            current=queue.get()#(cost,node,path)
            
            if (current[1].getName() in destNames):
                return [current[0],current[2]]#cost and path

            if current[1] in visits :
                if current[0] < visits[current[1]][0]:    
                    visits[current[1]] = [current[0],current[2]]
                    
                else:
                    continue
            else:
                visits[current[1]]=[current[0],current[2]]
                
            print(" - - - Search Process " + str(current))#search process
            lst = current[1].getNeighbors()
            
            for i in lst: #i=[Node,int cost, str nodeName]
                try:
                    if visits[i[0]][0] > current[0]+i[1]:#stored cost > cost going in?
                        queue.put((i[1] + current[0], i[0], "{} {}".format(current[2],i[2])))
                        
                except:
                    queue.put((i[1] + current[0], i[0], "{} {}".format(current[2],i[2])))

def readFromCSV(path):
    f = open(path, "r")
    return f.read().splitlines()

def createGraph(dataJarak, dataLabs):
    graph=Graph()
    datas=readFromCSV(dataJarak)
    for i in datas: # i = "A,B,distance"
        edgeData = i.split(",")
        graph.addEdge(edgeData[0].upper(),edgeData[1].upper(),float(edgeData[2]))

    datas=readFromCSV(dataLabs)
    for i in datas: # i = "Kec,LabName"
        labData = i.split(",")
        graph.addLab(labData[0].upper(),labData[1])
    return graph


dataJarak = "TK.csv"
dataLabs = "Labs.csv"
graph=createGraph(dataJarak,dataLabs)

print("\n==============================================================================================================")
print("==Data laboratorium pemeriksa covid-19 berdasarkan www.litbang.kemkes.go.id/laboratorium-pemeriksa-covid-19/==")
print("==============================================================================================================\n")

while(True):
    inp = input("Masukkan lokasi mulai anda ")
    inp = inp.replace(" ","").upper()
    output = graph.pathFind(inp,graph.getLabLocations())
    path = output[1].split(" ")
    if(not output == [0.0,""]):
        print("\nKecamatan dengan laboratorium terdekat : " + path[-1])
        print("Jarak ke kecamatan tersebut            : " + str(output[0]))
        print("Jalan menuju kecamatan tersebut        : " + output[1])
        print("Anda dapat melakukan test pada         : " + graph.getLabName(path[-1]))
    print()
    
